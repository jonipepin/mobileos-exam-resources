package edu.usna.mobileos.sampleprojectexam2;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class MyIntentService1 extends IntentService {


    public static int NOTIFICATION_ID = 1775;

    public MyIntentService1() {
        super("MyIntentService1");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String source = intent.getStringExtra("source");

        String message = retrieveSiteContent(source);

        Intent broadcastIntent = new Intent();
        broadcastIntent.putExtra("message", message);
        broadcastIntent.setAction("UPDATE_COMPLETE");
        getBaseContext().sendBroadcast(broadcastIntent);

        generateNotification( this,
                "Beat Army",
                "the army goes rolling along",
                NOTIFICATION_ID );

    }

    public static void generateNotification(Context context, String title,
                                            String message, int notificationId) {
        int icon = R.drawable.ic_launcher;

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                context).setSmallIcon(icon).setContentTitle(title)
                .setContentText(message);

        // TODO #2 START: Set the notification properties









        // TODO END

        Intent notificationIntent = new Intent(context, MainActivity.class);

        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

        mBuilder.setContentIntent(intent);
        notificationManager.notify(notificationId, mBuilder.build());
    }


    // DON'T TOUCH ANYTHING BELOW HERE //
    public String retrieveSiteContent(String siteUrl) {
        // simulate getting content from the given source
        // assume this is the best code you've ever seen
        return "Go Navy!";
    }
}
