package edu.usna.mobileos.sampleprojectexam2;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    TextView displayText;
    Button button1;

    long alarmSeconds = 5;

    AlarmManager alarmManager;
    Intent intent;
    PendingIntent pendingIntent;

    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        displayText = (TextView) findViewById(R.id.displayText);
        button1 = (Button) findViewById(R.id.button1);

        button1.setOnClickListener(this);

        IntentFilter intentFilter = new IntentFilter();

        // TODO #1 START: Set the proper action for this intent filter

        intentFilter.addAction("______________________");

        // TODO END

        registerReceiver(intentReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // TODO #5 START: cleanup
       





        // TODO END
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_update) {
            Intent intent = new Intent(getBaseContext(), MyIntentService1.class);
            intent.putExtra("source", "mccoy");
            startService(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view){

        alarmManager = (AlarmManager)getBaseContext().getSystemService(Context.ALARM_SERVICE);
        intent = new Intent(this, MyReceiver.class);
        intent.putExtra("source", "hatfield");

        pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + alarmSeconds * 1000, pendingIntent);


    }

    private BroadcastReceiver intentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String result = intent.getStringExtra("message");

            displayText.setText(result);

            if(result.equals("Go Navy!")){
                playAnchorsAweigh();
            }
        }
    };

    public void playAnchorsAweigh(){
        // TODO #4 START: play anchors aweigh
        








        // TODO END
    }

}
