package edu.usna.mobileos.sampleprojectexam2;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class MyIntentService2 extends IntentService {


    public static int NOTIFICATION_ID = 1775;

    public MyIntentService2() {
        super("MyIntentService2");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String source = intent.getStringExtra("source");

        String message = retrieveSiteContent(source);

        Intent broadcastIntent = new Intent();
        broadcastIntent.putExtra("message", message);

        broadcastIntent.setAction("UPDATE_COMPLETE");

        getBaseContext().sendBroadcast(broadcastIntent);

        generateNotification( this,
                "Go Navy",
                "anchors aweigh",
                NOTIFICATION_ID );

    }

    public static void generateNotification(Context context, String title,
                                            String message, int notificationId) {
        int icon = R.drawable.ic_launcher;

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                context).setSmallIcon(icon).setContentTitle(title)
                .setContentText(message);


        Intent notificationIntent = new Intent(context, MainActivity.class);

        // TODO #3 START: Ensure a new MainActivity is not created if one already exists
        






        // TODO END

        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

        mBuilder.setContentIntent(intent);
        notificationManager.notify(notificationId, mBuilder.build());
    }


    // DONT TOUCH ANYTHING BELOW HERE //
    public String retrieveSiteContent(String siteUrl) {
        // simulate getting content from the given source
        // assume this is the best code you've ever seen
        return "Beat Army!";
    }
}
