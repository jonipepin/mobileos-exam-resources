package edu.usna.mobileos.sampleprojectexam2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by pepin on 4/1/15.
 */
public class MyReceiver extends BroadcastReceiver {

    public void onReceive(Context context, Intent intent){

        String source = intent.getStringExtra("source");

        Intent i = new Intent(context, MyIntentService2.class);
        i.putExtra("source", source);
        context.startService(i);

    }
}
